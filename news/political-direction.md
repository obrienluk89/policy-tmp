---
title: "Political direction"
description: Politics is a lonely business and I have chosen to join a party, read about my decision here.
image: img/direction.jpg
date: 03-03-2025
---

# Political direction.

##

Politics can be a lonely business and it is hard on your own.\
Independents may do well in local elections where a ward is a district of a town, this is quite a manageable size to cover as an independent, but a Westminster constituency for national government is roughly 70,000 people, that is much more difficult and less manageable as an independent, Suffolk and Norfolk could also see an [elected mayoral position](https://www.gov.uk/government/consultations/norfolk-and-suffolk-devolution/norfolk-and-suffolk-devolution-consultation), this would be a combined size of over 1 million people.

It is clear then that if I intend to continue in politics then I must join a party.

Being an independent does have its advantages, as an independent you are neutral, everyone is willing to talk to you and your reputation is your own.

But when I join a party, I would inherit the reputation of that party and I would be in danger of losing my independence.

Whatever party I choose, it is inevitable that I will disappoint somebody, during my general election campaign and my time as a councillor, I have made an impression on my fellow councillors, all of whom would be very happy to welcome me into their respective parties.

However, I have made my decision to join Reform UK.\
There may be some who question my decision and so I wanted to write an article explaining some of my reasoning.\
The three main reasons are:

::: accordion

### Ukraine

The war in Ukraine is somewhat personal to me, I have visited Ukraine and I have written about [the betrayal of Ukraine](https://luketobrien.uk/news/ukraine-betrayal/) in a previous article.

Liz Truss was Foreign Secretary at the time, her failed attempts at negotiation resulted in the war and Boris Johnson interfered with the peace process in 2022.

Ukraine is one of the poorest countries in Europe and Russia is a nuclear superpower.

It was a terrible mistake to encourage Ukraine to fight this war and politicians have made no attempt to end this conflict, instead they believe their own exaggerated rhetoric that has misled people to join the foreign legion.

The behaviour of our politicians is unforgivable and proves that they are either incapable or unwilling to solve the problems in our own country.

When foreign policy mistakes lead to the deaths of hundreds of British soldiers then that is one thing, but when foreign policy mistakes lead to the deaths of hundreds of thousands belonging to another country then that is quite another.

Unfortunately, this current Labour government is continuing to follow the same mistaken path laid by the previous government, and now, with a new chance for peace, the government still seems to be reluctant or even undermining of that peace effort.

However, I am in more agreement with Reform's position on the war.

Nigel Farage and [Richard Tice](https://news.sky.com/video/will-ukraine-need-to-give-up-land-to-russia-as-part-of-potential-peace-deal-13308022) have received a lot of criticism for their comments, and of course I would give my own perspective, but their comments are correct and [Tice displayed great self-control during interviews](https://youtu.be/rWIYquyJI5Y), this will be to their credit in the future.

### Immigration

There are legitimate concerns about immigration and those concerns are not being listened to.

The genesis of these concerns is a report published in the year 2000 by the United Nations called ['Replacement migration : is it a solution to declining and ageing populations?'](https://digitallibrary.un.org/record/412547).

The United Kingdom, like many developed Western nations has suffered with a decline in the working aged population due to a decline in fertility.\
'Replacement Migration' refers to using migration as an economic tool to compensate for a population decline and provide a workforce for an ageing population.\
This is an unwise strategy as it does not address but rather masks the underlining health and social problems that cause societal decline.

Replacement migration is unsustainable, a declining population is an unhealthy population, it is imperative that politicians find solutions that can heal our unhealthy society and encourage natural population growth.

[Matt Goodwin](http://www.matthewjgoodwin.org/) explains the decline that Britain is going through very well in the video below:

<iframe width="560" height="315" src="https://www.youtube.com/embed/HNeR8lHj6TI?si=O-BFP32Z3jF_M4sT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

**Response to the immigration riots**

---

The way in which the government managed the immigration riots that took place in the Summer of 2024 was misguided.\
They were treated as race riots and put down quickly with no consideration given to people's grievances.

There is much despair in the country generally and British people are not unaware of societal decline, they feel that pain and I feel it too.

Immigration has brought crime and terrorism to this country.

The town of Rotherham was the centre of the grooming gang scandal, so the rioters were understandably angry and that anger needed to be taken into consideration when managing a measured response, but no consideration was given.

Their anger was also fuelled by the memory of terrorist atrocities, people still remember Lee Rigby and every other atrocity that has happened since, these memories will never go away, they are national tragedies and blots on our nation's history.\
Whenever I travel to Manchester and visit the [Glade of Light memorial](https://www.manchester.gov.uk/info/500369/glade_of_light_memorial/8206/the_glade_of_light_memorial) I am saddened that this happened in my country, this is permanently recorded in history and will never be undone.

People feel like their country is dying and these were the feelings that drove [Peter Lynch](https://www.bbc.co.uk/news/articles/c3vlwxw6wweo) to take his own life.

This man was arrested for violent disorder during the riots, but this was verbal violence, not physical violence, he shouted and held a placard, he did not deserve to be arrested.

When a man commits suicide, that is a tragic event that should cause us shock, to sit up and to pay attention to these pleas of pain.

I hear these pleas and I feel this pain.

### A chance for something new

Voters throughout the United Kingdom and Europe are stuck.\
There is a term called 'politically homeless' and we just don't know which direction to turn.

Politics aside and looking at Reform purely on paper, we can see an emergence of a new political party and joining this new party is a chance for something new.

Over time, years and decades, Reform will grow, as more people join Reform, bringing their own talents and political opinions, the party will mature and will become more refined.

It is very possible that the Reform party of the future would be very different from the party of today.

The future is the undiscovered country, I am excited about what we can discover and what better future we can build for our country.

:::

So as I join my new party and continue along my political journey, I hope voters will be understanding of my decision and will still feel like they can approach me.

This is an exciting chapter in my journey and I look forward to meeting new friends along the way.