---
title: "Majesty: Happy & Glorious"
description: The King's coronation is a happy and glorious occasion - Read my thoughts on the day and my opinion of monarchy.
image: img/happy-glorious.jpg
category: Articles
---

# Majesty – Happy & Glorious

The coronation of King Charles III was a wonderful event to see, it was the first time in most of our lifetimes that we witnessed the coronation service and due to the internet age we live in, it was the most accessible service ever seen, viewed, I’m sure, by millions of people around the world, it will be watched over and over again by each generation to come.

I especially liked the details of the service, the start of the service was significant.

> [Order of Service](https://www.bbc.co.uk/news/uk-65503950).

The King was greeted and asked what are you doing here?

> “Your Majesty, as children of the kingdom of God we welcome you in the name of the King of kings”

To which the King replied:

> “In his name and after his example I come not to be served but to serve.”

This simple act of humility is evident throughout the service.

Christian theology is ever paradoxical, Jesus Christ is the King of kings, that is all earthly kings and rulers are subordinate to him, but Jesus was a servant King, he gave his life to heal and give hope to others and so our earthly king echoes this example.

One of the good aspects of a constitutional monarchy is that that there is no one person who is supreme or who takes all of the glory, our king is subordinate to an invisible, or more specifically, hidden God who reigns in heaven, our king plays his part here on earth but all glory is redirected to what is hidden.

This is a very interesting arrangement and one that we are fortunate to have, it is an abstract system of rulership and one that prevents personal ambition or attention.

By contrast we can look at China whose leader, Xi Jinping has a personality-cult built around him, much the same as his predecessor Mao Zedong did.

In fact it has been the tendency in many atheist-led regimes in history, North Korea has a supreme leader who takes all the glory for himself as did Nazi Germany, however our supreme leader is not of this earth and so no one man takes the glory, this is the shared heritage of all European nations.

However, in the media and career-driven political world that we now live in there is a danger that we are slipping into personal ambition and glory-seeking. It seems as if many politicians are trying to become celebrities, who simply appeal to people in order to see their own ambitions achieved, who make videos and take photos not to inform the electorate but to amuse their social media followers. These politicians would tell you that ‘up’ is ‘down’ and that ‘down’ is ‘up’ but they do not lead in any direction.

I believe that we all should learn this paradoxical lesson, particularly politicians, to lead by serving others and not seeking glory but to redirect that glory to where it is hidden. All political leaders, no matter their faith, should all strive to be humble **servant leaders**.

As I talk about the importance of what is hidden, it is apt and right that the most important part of the service is hidden from view.

The most important part of the service was when the King was anointed with holy oil, he was disrobed and sat behind decorated screens whilst oil was placed upon him.

I think the theatrics of bringing in the screens must also make this one of the most memorable moments of the service.

> [Watch the anointing](https://youtu.be/Taf2haEMsSQ?feature=shared).  
> [David Starkey explains the ritual](https://youtu.be/2tPq-6azd4k?feature=shared).

To enter into a life of service is a sacrifice, for a king this may be a small sacrifice to make, but through his many charities the King and the Royal Family recognise the sacrifices that many people make every day.

Service is also a self-less act and this is something that is much in need in today’s society.

We live in a society that is very career-driven, people are concerned with their own self interests, their own ambitions and the want to enrich their lives, but ‘career is a necessary evil’, we all need to live and pay the bills but it is not the meaning of life, ‘life is a sacrifice’, this is something that parents of disabled children know all too well.

Volunteering is an act of service, the selfless giving of our time, talents or financial donations, so it is wonderful that the coronation celebrations concluded with [The Big Help Out](https://www.thebighelpout.org.uk/) on the 8th of May, this initiative plans to reinvigorate the selfless volunteering spirit and remind the nation what life is really all about.

Another detail that I liked was the ‘Happy & Glorious’ banner hanging from Admiralty Arch.

It reminded me of one of my favourite songs, 'The Good Humor Man He Sees Everything Like This', it is a reminder of a more humbler existence, not to be motivated by career but to take joy in the simple things in life, to honour your King and to be happy. This is what the Royal Family represents and this is what national celebrations bring to communities all over the country.

I am also interested in the field of Ethics and in virtues, one in particular is the virtue of ‘Magnificence’. Aristotle describes ‘Magnificence’ as the spending of large amounts of money for the benefit of others.

The King’s coronation was Magnificence on display for the whole world to see, it certainly did cost a large amount of money and it did benefit a large amount of people, so I’m sure Aristotle would agree that the coronation was a virtuous event.

Another branch of philosophy is that of Metaphysics, ‘Meta’ meaning ‘addition to’.

There is more to life than this physical world, Happiness, Beauty and Imagination are all in addition to the physical world, we humans cannot live without these additions, the monarchy adds colour to British life, in fact the world, and we must cherish what beauty still exists in this world.

In fact some people are calling Charles III [the philosopher King](https://www.telegraph.co.uk/news/2023/05/08/charles-iii-is-a-21st-century-philosopher-king/).

::: accordion

## Opinion: Monarchy

England is a country that has undone a revolution, the result of the English civil war was a parliamentary victory but after five or so years of being a republic, the monarchy was re-established.

The reason the monarchy was re-established is due to some of the reasons I mentioned above, because no one man should be at the top and simply because a republic is less fun.

So it’s not just a question of ‘Should the monarchy be abolished?’ but also ‘Would it stay abolished?’ and my answer to both questions is ‘No’.

Even if the monarchy was abolished, the Royal family would still exist as a self-maintaining organisation that employs thousands of people and the monarchy would eventually be re-established again because people would grow bored and miss the Jubilees, Coronations and other celebrations that monarchy brings.

I also believe that calls to abolish the monarchy are a distraction.

[Labour has plans to abolish the House of Lords](https://www.bbc.co.uk/news/uk-politics-63692981) which is one step away from abolishing the monarchy itself, so I am opposed to this idea and this too is a distraction.

The monarchy and the House of Lords are not the problem, both of these institutions are restricted in their powers. The problem are politicians in the House of Commons, they are the ones who make decisions and laws that effect the lives of people in this country, they are the ones who are career ambitious and who seek attention.

Both the House of Lords and the monarchy are unique characteristics of this country, this is who we are and this is how we do things in our country and national identity is the biggest reason for the monarchy.

In addition, I think calls to abolish the monarchy are based on two false assumptions:

* The monarchy costs the taxpayer money.
  The funding of the monarchy is complex, but the [BBC has a helpful diagram here](https://www.bbc.co.uk/news/explainers-57559653).
  Under law, the monarchy does not have an income, instead all of the income from the Crown Estate goes directly into the treasury, the Sovereign Grant is a percentage of that income that goes to fund the official duties of the monarch.
  So if the monarchy was abolished then the treasury would lose the income from the Crown Estate and this would cost taxpayers money.
* The Royal Family doesn’t have any real jobs.
  Members of the Royal Family are ambassadors and diplomats, the reputation of the United Kingdom and our relations with foreign nations is greatly enhanced because of them.
  I believe that during the early years of Charles’s life, he did have a crisis of confidence and purpose, this led to tragedy and a strain on family relationships, but we can only forgive this tragedy and move on with a renewed sense of purpose.

:::