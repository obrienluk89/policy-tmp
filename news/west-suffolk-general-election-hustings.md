---
title: West Suffolk General Election Hustings
description: See my performance at the general election Hustings at Haverhill in Suffolk.
image: img/Luke-T-OBrien-Independent-Candidate.jpg
date: 27-06-2024
---

# West Suffolk General Election Hustings.

##

During the general election campaign in June 2024, I took part in a hustings event in Haverhill, Suffolk.

Here are my highlights from the West Suffolk General Election Hustings.
The hustings were organised by [Reach Community Projects](https://www.reachcp.org.uk/) and hosted by [St Mary's church](https://stmaryshaverhill.org.uk/) courtesy of Revd Max Drinkwater.

::: accordion

### Introduction

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=0)

### Q1: What are the needs of Haverhill and what are your plans?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=178)

### Q2: Will food banks always be needed?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=304)

### Q3: What are your plans for infrastructure in Haverhill?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=425)

### Q4: To whom do you serve? Self, party, country, voters?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=507)

### Q5: How will you support mental health professionals in schools?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=600)

### Q6: Will you support charities in Haverhill?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=736)

### Q7: Would you vote against your electorate even if they disagree?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=810)

### Q8: Do you have experience of local government in West Suffolk?

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=920)

### Closing statement

![](https://www.youtube.com/watch?v=rMZJkEI-37k&t=1007)

:::