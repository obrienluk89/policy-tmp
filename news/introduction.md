---
title: "An outsider's take on politics"
description: I am a unique character with a lot to offer - My introduction article.
image: img/luke-gate.JPG
background: aliceblue
date: 07-12-2023
---

# An outsider's take on politics.

##

My name is Luke T O’Brien and I am standing as an independent candidate in West Suffolk, which some of you might know as our current MP, Matthew Hancock, has become infamous.

I am not the usual sort of person you would expect to stand to become a politician but I believe that I am a unique character that has much to bring to the world of politics.  
My life has been something of a minor miracle, I have multiple disabilities but despite my struggles I work as a Software Developer and live a good life here in Suffolk.  
I was diagnosed with Cerebellum Ataxia at the age of 5, I also have the learning disabilities, Dyslexia and Discalculia (maths disability), as you can imagine it has been a long path to a successful life, however now I can see my disabilities not as a weakness but as a strength.  
I have read numerous articles over the years discussing how little representation for disabled people there is in parliament and, if elected, I could certainly change that.  
Of course one doesn’t have to be disabled to have empathy but I have first-hand experience of what it is like and I especially understand the sacrifices made by the parents of a disabled child, this is very important to remember, disability doesn’t just affect the disabled person themselves but all those who care for them.  
I understand that for many people life is a sacrifice, parents, especially those of disabled children, sacrifice for their children, again, teachers, especially learning support teachers, sacrifice for their children and of course our police and armed forces sacrifice when they are called to duty.  
It is essential that parliament understands and recognises the sacrifices that people are willing to endure for the benefit of others.

One of my social policies is to reinstate Disability Living Allowance (DLA), so if I am elected then I will certainly speak up for disabled people and their families.

Aside from greater awareness for disabled people, education is something that I am very passionate about.  
There has been an ongoing **mental health** crisis facing young people, to my knowledge, this crisis has been going on for around a decade and the recent Coronavirus pandemic only adds to the crisis.  
I have read many articles and watched interviews about a number of suicides all over the country and how this has affected parents who campaign to try and solve this issue.  
I believe that I have a unique experience that could add to a solution to this crisis and I can remember what it was like for me as a child.  
As all people do, I have often looked back over my life and asked myself the question "How did I get here?".  
It is know over-statement to say that my life is a minor miracle but the interesting thing that I can conclude from my life is that **weakness can be a strength**.  
For example, due to my physical disability I find mobile phones difficult to use, I never got into texting and even though smartphones add new features such as speech-to-text, I have never really gotten into social media ether.  
Although I do have a Twitter account, which I try to use in 'readonly mode' and I do not advertise on my website because I do not want to encourage social media use.  
Socially too, my relative isolation has made me thoughtful and has kept me free from influences.  
This is an important lesson for children to understand, sometimes weaknesses in the short-term can turn into strengths in the long-term, I can testify that the future is not what you expect and that you will be surprised with how it turns out, so it is important for children to be positive and be flexible about the future.  
I think that one of the reasons this mental health crisis is that children have a rigid and perhaps unrealistic vision of their own futures, rigidity that is unintentionally enforced by the education system with it's focus on targets, testing and exam results.  
Our current education system is firmly directed towards university, as a 16 year old, it was a path that I wanted to go along too, I did okay in my GCSEs, Cs in English and Science, D in Maths and a B in Drama, at Sixth Form I retook my Maths (2 or so times) and got a C, but after 1 year of Sixth Form I failed my AS levels and went to college, it was here that I realised that university isn't the only path and that C grade is not the minimum, whereas C was the minimum required for Sixth Form courses, at college it was possible to do courses with a E grade minimum.

So when it comes to education reform, I am against the over-testing of children, at certain times it is important to know how children are getting along, for instance when making the transition from primary to secondary education, but too much testing can be burdensome on both teachers and children alike.  
Another way policy makers can improve the mental health of young people is by improving the **National Curriculum**.  
I can remember from my experiences that children do question the purpose of learning and so there needs to be practical reasons why subjects are taught, one of my education ideas is to incorporate PSHE into the main subjects because PSHE doesn't lead anywhere, you cannot study PSHE at university or get a job as a PSHE Engineer, so I expect for many children it has little value.

In 2015 the then Education Secretary Michael Gove introduced a much needed practical subject that is close to my heart, GCSE Computer Science.  
By introducing Computer Science into the national curriculum the government stumbled on a gold mine.  
Computers are a box of wonders, not just a single invention but a whole host of inventions that leads to an entire universe of opportunities – Career and otherwise – Computers have given me a life, if I was born in previous decades then I wouldn’t of survived.  
Computers are also one of the reasons why the USA is the biggest economy in the world, Microsoft, Google, Apple, they are all American software companies, of course Apple also builds its own hardware and they are joined with the likes of Dell and Hewlett-Packard, the second biggest economy in the world, China, also has computing to thank for its success, China has a closed internet which means that it has its own alternative companies, there is a Chinese alternative to Google and eBay, Huawei is also one of the world’s largest manufactures of hardware.  
So computing is definitely something that needs to be embraced on the national curriculum that will greatly benefit the future economy.

However, designing the subject content for GCSE Computer Science is a little more specialised than most subjects.  
Most people know the important periods of history that children should learn, but the important aspects of computing are not common knowledge.   
If I was to enter parliament as an MP then I would look forward to sharing my knowledge and insights, I am sure whoever is in government after the election would value my suggestions for improvement.  
I strongly support the inclusion of Software Engineering principles within GCSE Computer Science, here is an opportunity to teach logic to students, many children who struggle with maths may very well excel at logic, structure and order, software development is also a very creative pursuit and many children will find it exciting, as I’m sure they also would enjoy building computers from components and playing with network cables.

Having your own policies is one of the advantages of being an independent candidate, whilst party candidates inherit the policies of their party, I am free to create my own.  
I also hope that my efforts will be appreciated by the people of West Suffolk, most politicians cannot create their own website but I have not only created my own website but also my own [static website generator](https://www.cloudflare.com/en-gb/learning/performance/static-site-generator/) to generate it with, I have also designed my own [flyer](https://luketobrien.uk/Flyer.html).  
Conservative voters also do not have anything to fear from me, I have similar interests to some current MPs, Michael Gove introduced GCSE Computer Science, which I am very keen about, Ian Duncan Smith has connections to [Save the Family](https://www.savethefamily.org.uk/) and I saw him give a speech at Chester cathedral, I have previously written to Conservative MPs, my farther still writes to Thérèse Coffey, so I am sure I will be warmly welcomed and my input on discussions would be much appreciated.

I am also confident that I would be warmly welcomed in the House of Commons and I am confident that I will win support and make some progress with some of the issues I care about, particularly the environment, education and mental health.  
So I believe I am a unique character who has a lot to offer this country and I am confident that I could win the next election, we have elections every five years so why wouldn't anyone take the chance on a disabled person? - After five year's you could always have the Conservatives back again.  
<!-- 
---

Another reason I felt compelled to write this article is in response to ex-minister [Perry O'Neill’s recent decision to support the Labour party](https://www.theguardian.com/politics/2023/jan/09/ex-tory-minister-quits-party-and-lavishes-praise-on-starmer) and her comments.  
Some of what she said seems to me to be untrue and this may lead people to have a misunderstanding about the Labour party and it’s leader, so I wanted to give people my opinion.

In her article, O’Neil criticises the Conservative party of being “self-obsessed” and dominated by “ideology”, the truth as I see it is that both parties are guilty of these things and I believe that the Labour party is even more dominated by ideology.  
She praises Sir Keir Starmer saying that he offers “sober, fact-driven, competent political leadership”, but he can never seem to make up his mind on issues and tries to please everybody whilst pleasing nobody.  
To me the most telling interviews with Sir Keir have been on LBC radio with Nick Ferrari: [Keir Starmer says 'vast majority' of women 'don't have a penis' and need safe spaces](https://www.lbc.co.uk/news/keir-starmer-says-vast-majority-of-women-dont-have-a-penis-and-need-safe-spaces/) and [Starmer grapples with Labour's stance on trans rights and speaks out over 'intolerance'](https://www.lbc.co.uk/news/keir-starmer-lashes-out-intolerance-transgender-issues/), on the subject of transgender, another concerning opinion expressed was that of Lisa Nandy MP who believes 13 year old children should be allowed to change gender: [Labour’s Lisa Nandy suggests 13-year-olds should be 'taken seriously' if they want to change gender](https://www.telegraph.co.uk/politics/2023/01/19/labours-lisa-nandy-suggests-13-year-olds-should-able-change/).  
During interviews, Keir Starmer wants to give an answer that will please everybody but to me it seems he struggles with basic facts, he has also been hypocritical in condemning MPs for having second jobs whilst defending his own MPs.  
Not mentioned in her article but the biggest reason to be skeptical of Labour is their plans to further devolution and give more powers to Nicola Sturgeon: [Keir Starmer pledges a 'transformed state' as Labour to hand power to Sturgeon's Scotland](https://www.express.co.uk/news/politics/1717243/Keir-Starmer-speech-Labour-SNP-power-Nicola-Sturgeon-Scotland-devolution-news), Keir Starmer has also recently given in to the demands of ‘Just Stop Oil’ and has pledged to end oil exploration in the North Sea: [Labour will end North Sea investment, says Sir Keir Starmer](https://www.telegraph.co.uk/business/2023/01/19/labour-will-end-north-sea-investment-says-sir-keir-starmer/).  
On top of all this, I believe that this current Labour party is the most unprofessional in living memory, many Labour politicians post silly photos of themselves on social media and Angela Rayner in particular doesn’t at all behave like a serious politician, [she used her legs to distract Boris Johnson](https://www.bbc.co.uk/news/uk-england-62113335), played the DJ at a charity event, frequently underdresses for parliament and uses [inappropriate language in the chamber](https://www.bbc.co.uk/news/uk-politics-54638267), the Labour party has even turned against one of it’s own MPs: [Rosie Duffield: Being in the Labour Party is like an abusive relationship](https://www.telegraph.co.uk/politics/2023/01/20/rosie-duffield-labour-party-like-abusive-relationship/).

Again with education, Labour is planning to end the charitable tax status of private schools, a move that will likely increase the fees that parents have to pay.  
I believe that the state should let people be, if parents want to send their children to private school then good luck to them and good luck to their children.  
I am sure that many parents are not vastly wealthy and they may work second jobs or sacrifice in other ways for their child’s education, and to be fair, the education of children is a charitable cause, so perhaps the charitable tax status is justified?  
Some private schools provide for disable children or those who have self-excluded themselves due to bullying, these schools rightfully deserve charitable tax status.  
With the money generated by this, Labour hopes to recruit more teachers, but more teachers will not improve education when the materials they have to work with are not good enough – I have mentioned above how GCSE Computer Science could be improved and in fact I believe the whole curriculum needs to be reviewed and redesigned.  
This also seems to me to be what is called, the **politics of resentment**, the resentment of people who send their children to private school is not a good basis for logical, reasoned policy.

So please be under no illusion, neither the Conservatives nor Labour have the answers and many of the issues I care about will not be addressed by either of them, but at least Rishi Sunak is an economist who has experience in the city, Keir Starmer is a lawyer, it’s not that I don’t trust lawyers but an economist seems a more logical choice to lead the economy.

So my advice to people, there is very little choice but the Conservatives are the only professional political party at the moment and there is no shame in voting for them.  
There is nothing worse then a betrayal and that’s what the Labour party has done to many working people in this country.
There is also an ongoing row regarding MPs expenses in which Labour is just as much guilty of abusing as the Conservative party: [Why Labour’s attack on Tory expenses will come back to bite them](https://www.telegraph.co.uk/columnists/2023/02/14/why-labours-attack-tory-expenses-will-come-back-bite/).  
It seems that MPs expenses are seen as an extension to their salary, a former MP even use his expenses to fund his cocaine habit: [Labour MP Jared O’Mara guilty of expenses fraud to fund cocaine habit](https://www.independent.co.uk/news/uk/crime/labour-mp-jared-o-mara-cocaine-b2278783.html).

But if you live in West Suffolk and you agree with me, then you can vote for me, I promise that I will only claim expenses for what they are designed for, travel and hotel accommodation costs, perhaps I could claim for a new church roof but never personal items, if elected and depending on the success of my campaign then I will have no choice but to establish my own party and expand beyond Suffolk.  
If I do win this election then it will be another miracle and I hope a humbling experience for established politicians, a time for them to pause and take notice of the little details of life, because some of the little details can bring a lot of happiness.
-->