---
title: "How Ukraine was betrayed"
description: Ukraine was betrayed, but not in the way it appears. Lessons need to be learnt and politics must be better as a result.
image: img/war.jpg
date: 18-02-2025
---

# How Ukraine was Betrayed.

##

There has been much news coverage about Donald Trump’s peace initiative to end the war in Ukraine and some news outlets are even going as far as to say that Ukraine is being betrayed.

The reason that some journalists say that Ukraine is being betrayed is due to [territorial concessions](https://www.ibtimes.com/pete-hegseth-ukraine-borders-nato-3763425) and the fact that NATO membership, which is a key goal for Ukraine, has also been ruled out by the US Defence Secretary Pete Hegseth.

Hegseth himself, however, argues that he is being realistic, it is true to say that one can only give up what one owns and the sad fact is that after three years of war, Ukraine has lost over 20% of it’s territory, that is the unfortunate reality and one that is difficult to come to terms with, Ukraine cannot give up what it has already lost, it has to come to the realisation that any attempt to regain that territory would be futile.

Donald Trump and Pete Hegseth are not betraying Ukraine, they are telling some uncomfortable truths and in doing so they will end a war that has cost the lives of hundreds of thousands of people.
So far from betrayers of Ukraine, I would say that they are actually saviours.

Ukraine’s betrayal happened many years ago before Trump took office and it is a political scandal unlike anything in living memory.

US Secretary of State Marco Rubio said that it was [“Dishonest” to say Ukraine could completely destroy Russia](https://bnn-news.com/dishonest-to-say-ukraine-could-completely-destroy-russia-regain-crimea-says-rubio-264582), this is very much true and this begins to explain how Ukraine was betrayed.

For a UK audience, it is important to remember that governments have a tendency of exaggerating when it comes to foreign conflicts.

This began with Tony Blair’s exaggerated claims of weapons of mass destruction in Iraq, we must also remember that the wars in Syria and Libya were the catalysts for the migration crisis, the aftermath of the Libyan conflict saw [international slave markets](https://www.bbc.co.uk/news/world-africa-39567632) and a report by the [Foreign Affairs Committee](https://publications.parliament.uk/pa/cm201617/cmselect/cmfaff/119/119.pdf) that was very damning of the then Prime Minister David Cameron, who ironically later became Foreign Secretary.

The word ‘rhetoric’ means the art of persuasion, politicians can choose to persuade people with logical arguments and explanations but unfortunately the tendency has been to use exaggeration as a form of persuasion.

It was and exaggeration to say that Ukraine was winning the war.

Just over a year ago politicians were writing articles in newspapers saying that Ukraine was winning and all we had to do was give them more weapons and they would achieve victory.

But in reality, Ukraine was always suffering terribly, which begs the question, were politicians saying these things and making decisions against the evidence they received? What was the military intelligence saying about the war?

These same politicians also encouraged Ukraine to lower it’s conscription age, originally Ukraine had a conscription age of 27, that age was lowered to 25 at the bequest of Ukraine’s allies, politicians are calling for that age to be lowered still to 18, thankfully Ukraine has not done this.

It was an exaggeration to say that Russia wanted to conquer the whole of Ukraine or to go further into Europe, but what’s worse is that politicians actually believe it themselves.

Politicians were threatening World War III, they portrayed Ukraine as the defender of Europe and David Cameron even had the audacity to label peace talks as ‘appeasement’.

These exaggerations have misled thousands of people to join the Ukrainian foreign legion.

Recently, a former Conservative Member of Parliament, Jack Lopresti, joined the foreign legion.
Amongst his reasons for joining was the idea that [“Putin won’t stop, this is a battle for Europe”](https://news.sky.com/story/former-tory-mp-speaks-from-ukraine-after-joining-military-following-end-of-political-career-13305958).
I’m afraid this reasoning is based on a false premise and the current leader of the Conservative party, Kemi Badenoch, really needs to get in contact with him to set the record straight, that is assuming she does not believe these exaggerations herself.

The failure that has led politicians to these conclusions is the failure to understand the separatist regions of the Donetsk and Luhansk people’s republics.

These regions have been dismissed by politicians as an excuse for Russian aggression, which may be the case but these separatist regions do exist.

These two regions emerged in response to the 2013 Euromadian pro-European Union protests.

Independence movements exist in Western Europe and so it is realistic to understand that these two regions are opposed to the European Union and that they want a different direction for themselves.

It is also important to remember that officially Russia is called the Russian Federation, so then we can logically understand that these separatist regions do not want to join the European Union but would rather join the Russian Federation.

David Cameron once said that the whole world is watching.

Yes it is true and it is very embarrassing.

The betrayal of Ukraine happened long before David Cameron took up office however.

It was Liz Truss who was the Foreign Secretary at the time of Russia’s invasion, her attempts at negotiation were described as the [deaf talking to the dumb](https://www.telegraph.co.uk/world-news/2022/02/10/russias-top-diplomat-mocks-deaf-liz-truss-testy-joint-appearance/), I’m afraid that we do have reason to question the behaviour of this particular politician and the outbreak of war is a disastrous outcome of any negotiation.

Another politician who we can be sceptical of is Boris Johnson.
A few months after Russia's invasion, and whilst peace negotiations were ongoing, Boris Johnson visited Kyiv, some would say [with a message](https://www.commondreams.org/news/2022/05/06/boris-johnson-pressured-zelenskyy-ditch-peace-talks-russia-ukrainian-paper).

Whatever Boris Johnson said in Kyiv, it didn't help to achieve peace, as not long after his visit the peace negotiations broke down.

An interview with [Ukraine’s chief negotiator Davyd Arakhamia](https://www.dagens.com/news/surprising-revelation-russia-ukraine-conflict-could-have-ended-just-months-after-its-onset), confirms that Boris Johnson did play a part in the breakdown of peace negotiations, he may have been the messenger as this was a collective decision by the West.

The best way to help Ukraine would have been to avoid this war, failing that then everything should of been done to achieve peace and it is better to achieve peace sooner rather than later.

Analysis by a German newspaper [published in English by Ukrainian media](https://www.pravda.com.ua/eng/news/2024/04/27/7453222/) concluded that: **“If the war had ended about two months after it started, it would have saved countless lives.”**

The war in Ukraine has exposed deep flaws in both politics and journalism.
Western leaders took a massive gamble by encouraging an impoverished nation to fight a prolonged war against a nuclear superpower, with devastating consequences.
Meanwhile, much of the media failed to challenge exaggerated narratives, instead presenting the conflict as a simplistic story of good versus evil—one that ignored the harsh realities on the ground.

If history teaches us anything, it is that wars are rarely won by rhetoric alone.
Ukraine has suffered immensely, and its people deserve better than political grandstanding and misleading optimism.
Moving forward, Western leaders must learn from these mistakes and ensure that foreign policy is grounded in realism rather than wishful thinking.
Journalism, too, must do better—reporting the news as it is, rather than shaping it into a marketable story.

The true betrayal of Ukraine was not in the search for peace, but in the years of misinformation and false hope that prolonged this tragedy.
If the war had ended sooner, countless lives could have been saved. Now, as a peace deal seems inevitable, the focus must shift to rebuilding Ukraine and ensuring that such costly miscalculations are never repeated.

For this reason, history may judge today’s political figures very differently from how they are seen now.
Donald Trump could well be nominated for a Nobel Peace Prize if his efforts successfully bring an end to the war. Meanwhile, leaders like Viktor Orbán, who faced criticism for urging restraint and his own attempts at peace, may be owed an apology — though it is unlikely they will receive one.

For me, the war cannot end soon enough.
I will be among the first visiting Ukraine and I will support its economic recovery not least by buying an ample supply of chocolates which the UNESCO World Heritage city of Lviv is famous for.

::: accordion

### Lessons from the Syrian conflict (2011 - 2015)

I have mentioned previous conflicts above, most notably Libya and Syria.  
Here I want to focus on Syria and compare this conflict with Ukraine.

The [BBC published a critique](https://www.bbc.co.uk/news/magazine-33997408) of the West's policy towards Syria.  
Another critique with more striking comparisons can be read on Foreign Policy (FP) magazine website [What the West Got Wrong in Syria](https://foreignpolicy.com/2017/08/22/what-the-west-got-wrong-in-syria/).  
Reading these articles I can see similarities between the strategy in Syria and the approach taking in the Ukrainian war:

> The West’s approach to the Syrian uprising was from the very beginning dominated by an overdose of wishful thinking.

It was wishful thinking to believe that one of the poorest countries in Europe could achieve 'victory' over a nuclear superpower.  
The term 'victory' was the wrong term to use, it is not about winning or losing but simply achieving a peaceful end to the war.

It is interesting that FP's article is an edited extract from Nikolaos van Dam’s book, [Destroying a Nation: The Civil War in Syria](https://www.amazon.com/dp/1784537977).  
In a few years time I imagine the same author could write a new book entitled 'Destroying a Nation: The War in Ukraine'.

:::