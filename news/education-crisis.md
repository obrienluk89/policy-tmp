---
title: "Education system in crisis"
description: Read how my education policies address many of the concerns raised by an independent review.
image: img/education.jpg
date: 04-07-2023
---

# Education system in crisis.

##

Recently I read the report [Better Schools: The Future of the Country](https://www.collegegreengroup.com/wp-content/uploads/2023/06/Tim-Clark-One-million-votes-Final-20_06_2023.pdf) by the political consulting firm [College Green Group](https://www.collegegreengroup.com/) and I wanted to write an article expressing how I think my [education policies](https://luketobrien.uk/policy/education/) can address some of the issues raised.  
As you might be aware, there are a number of problems in our education system and this has led to [a record number of teachers leaving the profession](https://www.theguardian.com/education/2023/jun/08/teachers-england-schools-figures-department-education-survey), I believe that unfortunately neither political party will have the answer but I hope I can contribute and work with any future government to solve these problems, I would say the biggest reason for teachers leaving the profession is due to poor behaviour in schools, as mentioned in the report, teachers can be subjected to verbal or physical abuse, so Labour's plan of giving teachers a pay rise by removing the charitable tax status of private schools wouldn't solve these behavioural issues.

::: accordion

### Point 1: Recruitment and retention.

One of my education policies is to faze out tuition fees with the aim of eventually abolishing them.  
I mention one way in which the government could achieve this would be to subsidise the tuition for certain areas, such as the teaching profession or nursing, in fact all public sector training should be looked at and this would greatly help with the recruitment of new teachers.  
I also mention that mature students should be encouraged, with many teachers leaving the profession it is important to re-train professionals who work in other industries and encourage them to become teachers, again the tuition for these mature students could be subsidised.

My policy of redesigning the national curriculum may well help boost teacher recruitment and retention as well as pupil behaviour, as part of this redesigning process a public consultation will be held, so professionals as well as parents will have a say on what is taught.

### Point 2: Workload.

I agree that the workload of teachers needs to be reduced, I do not believe in the over-testing of children and if I become an MP then this is something I will spend more time researching into.  
The **S** in **SOLID** originally stands for the [Single-responsibility Principle](https://en.wikipedia.org/wiki/Single-responsibility_principle) and so this is what I would like for teachers, indeed for all the public sector, they should focus on their single responsibility of teaching their chosen subjects.  
This is one of the reasons behind my policy idea to incorporate PSHE into the main subjects.

I believe that schools are providing a service to parents, parents are the primary educators of their own children and schools complement parents by teaching their children the academic and vocational subjects that they are not able to teach themselves.  
My mother was a physicist, not every parent is able to teach science or introduce their child to this subject, so this is the purpose of schools, to introduce to children what they wouldn't otherwise have the opportunity to learn.  
In recent decades the purpose of school has changed to include the teaching of 'Life in modern Britain', I believe that by doing this schools have taken on too much responsibility and this has in part led to the problems we now face.

The report mentions that teachers spend time on extra-curricular activities, even though some of these activities might be beneficial to children, teachers may in fact enjoy these activities too and of course they are well meaning but I wonder if these activities might be an additional burden which are outside of teacher's responsibilities.  
It is better for children to do extra-curricular activities with their parents than it is with teachers and this is really the parent's responsibility not that of the teacher.  
This is a question I could research further.

### Point 4: Pupil behaviour.

I believe that redesigning the national curriculum will have the greatest effect on pupil behaviour, I know that children do question the purpose of school and what they are learning, I also believe that children become frustrated and they take that frustration out on teachers.  
Listening to music from films and video games will feed into the interests of children. Listening to classical music, learning about encyclopedias and genealogy will introduce interests, culture and positive uses for the internet for children to enjoy.  
I imagine that my ideas on teaching computing will also engage with and perhaps amaze children.

The report mentioned a recent review of [Relationship and Sex Education](https://www.gov.uk/government/news/review-of-relationships-sex-and-health-education-to-protect-children-to-conclude-by-end-of-year).  
Relationship Education was introduced by the government to try and address the behavioual issues in schools, however it has been an absolute disaster, the subject was first taught in 2020 and already there is a review into it, given the fact that children have not been in school for 2 years because of the pandemic lockdown, for the government to have to conduct a review so early on is proof that the subject and policy have failed.  
I believe that PSHE should be incorporated into the main subjects and so Relationship Education should also be incorporated, the most logical way for 'Relationship Education' to be incorporated is by teaching **ethics** in Religious Education.  
Ethics is moral philosophy, the importance of and how to apply morals in society, I also question where children get there morals from when there are report of [rape culture in schools](https://www.bbc.co.uk/news/uk-56558487), so introducing children to morals and ethical principles is a good thing.

Sometime ago I also read another article about how [British children are malnourished compared with other European nations](https://www.thetimes.co.uk/article/e5538510-0f87-11ee-a92d-cf7c831c99b5), this is relevant to pupil behaviour and both physical and mental health.  
One of my policy ideas is to bring back milk for primary aged pupils, I believe this could help tackle malnourishment, calcium and vitamin D are very important for growing children, I also mentioned in my redesigning the national curriculum that 'Student should learn about vitamins and issues of deficiency', again I think this is something that will help children as they grow and to maintain a healthy life.  
[Labour has currently ruled out free school meals for all](https://www.thetimes.co.uk/article/labour-rules-out-free-school-meals-for-all-children-bpcz5r9ls) but there are many in the party who are calling for it.  
I do agree with free school meals for all, but I am not aware of the government's finances, perhaps it would not be affordable at this time.

### Point 5: Vocational and technical education.

I agree that many children are not being catered for, my idea of allowing children to study City & Guilds as an alternative to GCSEs would address this issue.  
Despite the introduction a vocational subjects GCSEs are academic and they will always be that way, allowing children to study vocational qualifications alongside or instead of GCSEs will cater for children who prefer vocational subjects over academic ones, it would also allow the GCSE curriculum to be streamlined as GCSEs will no longer include some subjects that are covered by alternatives.

I also believe this policy might prevent children from being excluded from school.  
Sometimes when children are excluded from school, they might then go on to study City & Guilds at college which they may find they are better suited to, allowing these alternative qualifications to be studied alongside GCSEs might help to avoid the trauma of exclusion.

### Point 9: Trans, LGBTQIA and gender issues.

I believe that the acronym LGBT[QIA] is unhelpful and shouldn't be used, we should treat people as individuals and labelling children as 'Trans' is also judgemental.  
With regard specifically to transgender, the government and all responsible authorities must be clear, all living organisms on this planet have either male or female gender and the DNA structure of any organism remains constant throughout that organism's life, as humans are living organisms therefore, humans are either male or female and it is not possible for someone to change their DNA.  
There have been reports of children [identifying as cats](https://www.telegraph.co.uk/news/2023/06/20/rye-college-children-neo-pronouns-cats-moons-rishi-sunak/) or [identifying as other animals](https://www.telegraph.co.uk/news/2023/06/19/school-children-identifying-as-animals-furries/), whilst these children might just be winding up authority figures, this is detrimental to education and delays maturity and it needs to stop.  
Teachers and children alike must be trusted and need to have the confidence to teach and study without the worry of being disciplined for saying what is true.

> To be disingenuous means pretending that one knows less about something than one really does.  
> Everyone knows the basic biological facts of life and so no one should pretend otherwise, especially politicians.

One of the recommendations in the report is:

> To provide absolute clarity on the terms 'sex' and 'gender'.

The words 'sex' and 'gender' can be used interchangeably, 'sex' is more of a biological term, whereas 'gender' is more formal and refers specifically to the category of male and female.  
The report also asks for clarity on the use of toilets.  
There have been reports of [schoolgirls being sexually assaulted in toilets](https://www.telegraph.co.uk/news/2023/06/28/essex-schoolgirls-sexually-assaulted-gender-neutral-toilets/), these tragic crimes should never have happened and obviously boys should not be allowed to use girls toilets or any other private spaces, I also believe that headteachers must be held accountable for any assaults that happen as a result of their decisions.

:::

## In conclusion.

I believe that I have some unique ideas which no other politician is talking about, I believe that some of my ideas would be welcomed by parliament and would make interesting debates.  
If I should win the next election then I look forward to opening up these debates and perhaps even influencing future government policy.