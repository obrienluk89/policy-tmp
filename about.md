---
title: About
order: 1
description: Read about me, who I am and what I could offer Suffolk if I were to become county councillor.
keywords: suffolk, luke, local
showItems: false
---

# About me.

## My Background

I was born in Ipswich hospital in the year 1985 and I grew up in the village of Martlesham Heath near Woodbridge.

Before I started school, I attended playgroup at the old World War II control tower, which is now a museum and is managed by the Martlesham Heath Aviation Society.

In 2007 I completed a BSc in Software Engineering, even though the degree was accredited from the University of East Anglia, I studied at (old) Suffolk College in Ipswich which meant that the tuition fees were less.

Since then I have been working for companies in Cambridge and Bury St Edmunds and I have become proficient in a number of programming languages.

## My Disabilities and My Accomplishments

At the age of five I was diagnosed with Cerebellar Ataxia, this is a physical disability which affects coordination and fine motor control, as well as this I also have Dyscalculia (Mathematics disability) and Dyslexia.

When I was at primary school I wore a helmet during PE lessons and I fell over often, I was not expected to achieve much in life and indeed at high school I continued to struggle, I missed several years for various reasons and when I was at school I did not use the bathroom or sometimes either eat nor drink throughout the day due to anxiety.

But now I have succeeded in life and I am an example of why you mustn’t judge anybody, as Shakespeare wrote, the future is the “undiscovered country”, you can only imagine but you have absolutely no idea what the future will be.

I am now a Software Engineer who is skilled in multiple programming languages, so I believe this experience would make me a good role model who is able to encourage young people along this path, I would also be greatly interested to improve the new GCSE Computer Science course content.

I feel very passionate about incorporating Software Engineering into the GCSE National Curriculum because computer programming has enabled me to have a good life and it can also enable others who are like me.
My life as a software developer has also given me experience in public speaking as a member of the Cambridge JavaScript and NodeJS meetup.

Aside from software development, I also have experience volunteering with the National Trust at Flatford and Dedham Vale.

Outside of the UK, I also volunteered at the Shepherd’s Field orphanage in Tanjin, China where I setup an IT room and taught students.

## My Family

Martlesham Heath is a model village that was built in the 1980s for the employees of the British Telecom research laboratories where my father worked. The village itself has a strong history as it is a former World War II airfield that was used as a testing ground for new aircraft and parts of the old runway can still be found underneath the heath.

The connection with World War 2 also runs in my family as my grandfather served with the Royal Signals during the war. At the beginning of the war he was evacuated from Calais before serving in North Africa, Sicily and ending the war in Italy.

London is my ancestral home, my parents are from Surrey/South London, my father grew up in North Cheam and my mother comes from Carshalton.

My father was born just after the Great Smog of London and remembers the last of the steam trains through the capital.

At the age of 16 he started working in the exchanges for Post Office Telecommunications, later moving into planning and management.

My mother was born in a prefab and worked as a physicist, she was one of the first girls in the country to study science, at a time when girls were segregated to study home economics, and she worked at the National Physical Laboratory in Teddington before moving to work at the Optical Research division at British Telecom.

My parents moved to Suffolk when they both transferred to new jobs at the BT Research Laboratories at Martlesham Heath.

My parents were keen mountaineers and birdwatchers so I spent a great deal of my childhood with nature as well as visiting Suffolk’s many historic churches, this gave me a great appreciation of nature and of the details of life which are often neglected in today’s society.

## Organisations & Charities I Support

I am currently a member of [The Round Tower Churches Society](https://www.roundtowers.org.uk/) and [The Family Education Trust](https://familyeducationtrust.org.uk/), regularly support [The Salvation Army](https://www.salvationarmy.org.uk/) and sponsor a child at an [SOS Children’s Village](https://www.sos-childrensvillages.org/) in India, prior to that a student at the [Tibetan Children’s Village](https://tcv.org.in/) in Dharamsala, India.

Around 2010 I traveled to Lang Fang just south of Beijing where I visited the Shepherd’s Field school and orphanage belonging to the [Philip Hayden Foundation](https://www.phfcaresforkids.org/) where I had been sponsoring a disabled girl called Tiffany, during the following years I also begun supporting [For Life Thailand](https://www.forlifethailand.org/) which is another organisation that supports disabled children.

I also supported the [Jatson Chumig School & Orphanage](https://mount4him.org/en/jatson-chumig-ang/) in Lhasa, Tibet.  
The video of the girls dormitory that the fund went towards can be seen on my Facebook page.

It was during this time that I also supported an organisation near Chester called [Save the Family](https://www.savethefamily.org.uk/) who work with vulnerable families who need help staying together.