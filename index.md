---
title: Independent candidate.
description: Independent candidate for Bury St Edmunds in the 2025 county council elections.
keywords: suffolk, election, independent, politic, local
label: Home
order: 0
header: "video/westminster.mp4 video/tower-bridge.mp4 video/england.mp4 video/sheep-wood.mp4 video/horse.mp4 video/church.mp4"
profile: profile.png
---


# Vote Luke!

2024 independent general election candidate for **West Suffolk**.  
2025 Suffolk County Council election for **Bury St Edmunds**.  

**1st May 2025**

* Disabled
* Software Engineer
* Leader

[About me](about)  [Priorities for Suffolk](priorities)


## Vote Luke