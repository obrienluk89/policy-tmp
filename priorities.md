---
title: Priorities
description: Read my priorities for Suffolk in 4 different areas.
template: list
order: 3
showItems: true
---

# Priorities

Here are my priorities for Suffolk.

Should I be elected to Suffolk County Council then these are some ideas I have to improve life here in Suffolk.

If you have any suggestions of how to improve Suffolk or have any comments of my priorities then please [Contact me](contact).