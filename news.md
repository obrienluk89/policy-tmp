---
title: News
description: Read articles that I have written about various political issues and the write ups of some of the events I have been to.
keywords: news, suffolk, local
template: list
order: 2
showItems: false
---

# News

Below are some articles I have written explaining my opinions on various political issue and I have also writing up some of the events I have been to.
