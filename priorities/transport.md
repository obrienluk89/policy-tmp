---
title: Transport
description: My Transport priorities for Suffolk.
image: img/transport.jpg
---

# Transport & Tech.

##

Transport is a big issue in rural areas of Suffolk where many elderly or disabled people can be isolated, improved rural transport is vital for these people in order for them to have a social life.

Many businesses also rely on transport connections and with one of the biggest container ports in Europe, transport is also vital for Suffolk's economy.

::: accordion

#### Rail improvements: Haughley and Ely junctions

I will add my voice to calls for improvements to the Haughley and Ely and I shall regularly ask for updates.

During the general election campaign, I was also contacted by the campaign group [Rail Future](https://railfuture.org.uk/).  
Should I become a county councillor for Suffolk, then I shall get in touch with this group and follow up on some of their ideas.

#### Suffolk transport plan

I will keep up to date with the [local transport plan](https://www.suffolk.gov.uk/roads-and-transport/transport-planning/suffolks-local-transport-plan) and I will make sure that improved rural transport is an important part of that plan.

:::