---
title: Environment
description: My Environment priorities for Suffolk.
image: img/environment.jpg
---

# Environment.

##


Suffolk is a beautiful and wonderful place to live.

I am a Suffolk boy through and through, I was born in Suffolk and I could never live anywhere else.

My parents are keen birdwatchers and environmentalists, I have fond memories of growing up here in Suffolk, visiting our many historic churches and walking in our wonderful natural spaces.

::: accordion

#### Recycling and Litter picking.

I would like to work towards 100% recyclability.

Currently, much of of the recyclable waste placed in blue bin does not actually get recycled.  
Some recyclable might be contaminated with food or some materials might be accidentally placed in the wrong bin.

The [Suffolk recycling website](https://www.suffolkrecycling.org.uk/) has a helpful guide that explains what can be recycled.  
But still there are many items that cannot be recycled at home.

I will conduct research and work towards improving recyclability and look at how recycling can be encouraged.

<hr/>

During my general election campaign, I took part in a litter pick with the [Lightwave community in Red Lodge](https://www.lightwavemission.org/) and I found it to be an enjoyable experience that the children particularly enjoyed.

The Suffolk recycling website has a [guide](https://www.suffolkrecycling.org.uk/what-more-can-i-do/litter-picking) of how organisations or groups of people can arrange their own litter pick.

If I am elected to Suffolk county council then I will encourage litter picking.

#### Beautification and Reporting tool.

Beautification is the process of tidying up vegetation, cleaning, conducting maintenance or in any other way making an environment more pleasurable.

I also believe that a good clean environment will indirectly benefit mental health and the development of children.  

Many urban areas in Bury St Edmunds and throughout Suffolk are in need of improvement and I will endeavor see that these needed improvements are completed.

Suffolk Highways has an [online reporting tool](https://highwaysreporting.suffolk.gov.uk/) which I commend and would encourage the use of.  
Even though this tool does have a reporting category for 'Grass and Hedges' it is not specifically and environmental reporting tool.

I will investigate the idea of creating a septate reporting tool for environmental purposes.

#### Wildlife.

My parents are keen birdwatchers and so I know that Suffolk is renown for it's birds and other such wildlife.

During my general election, I was presented with the [Suffolk Wildlife Trust's Manifesto for West Suffolk](https://www.suffolkwildlifetrust.org/sites/default/files/2024-06/West%20Suffolk%20Nature%20Manifesto.%20Suffolk%20Wildlife%20Trust%202024.pdf).

If I should be elected to Suffolk county council, then I shall follow up with the Suffolk Wildlife Trust and other organisations to make sure that Suffolk's most precious and natural assets are protected.

:::