---
title: Social
description: My Social priorities for Suffolk.
image: img/social.jpg
---

# Social.

##

Suffolk is one of the best counties in England to live in.

Here in Suffolk, we have low crime and a good standard of living, but there are always areas that need improvement.

::: accordion

#### Disability Confidence.

I will work with local businesses and other organisations in Suffolk to promote the government's [disability confidence scheme](https://www.gov.uk/government/collections/disability-confident-campaign) and I will also work with community group to help disabled people have confidence in themselves.

During my general election campaign, I was given a tour of the [Leading Lives](https://leadinglives.org.uk/) Midenhall hub and previously I have also worked with the [Papworth Trust](https://www.papworthtrust.org.uk/).  
These are two charities that I will get back in touch with and work together with them to improve the lives for disable people in Suffolk.


#### Health Stations.

During my general election campaign, I visited a [health station](https://www.sisuhealth.co.uk/) in Haverhill library.  
This health station gave me a very basic checkup that normally could only be performed by appointment only.

If I were to be elected to Suffolk county council then I shall explore this idea further and visit libraries to ask as to whether it is something that they would be interested in.

#### Rough sleeping.

During my election campaign, I came across of people sleeping rough, particullarly in church doorways.

If I should be elected to Suffolk county council then this is an issue that my office will research further into.


:::