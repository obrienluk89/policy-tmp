---
title: Education
description: My Education priorities for Suffolk.
image: img/education.jpg
---

# Education.

##

When it comes to education, I think about myself as a child and I think about my parents.

I try to imagine myself in today's education system, what benefits did I have that other children don't have? What struggles did I have?

The young people of today can certainly benefit from my experience and I have a lot to give.

Below are some of my thoughts on how I can improve education here in Suffolk:

:::::: accordion

#### Improve teaching of computing.

I work as a software developer and for a number of years I have been working on my own [software project](https://jollify.app).  
So I am really the ideal candidate to teach children about computers and computer programming.

One of the motivations for standing to be a MP was to improve the [GCSE Computer Science subject content](https://www.gov.uk/government/publications/gcse-computer-science).  
Being a county councilor would give me the opportunity to make improvements to the teaching of computing here in Suffolk before I am able to make the changes nationally.

One of the biggest challenges that teachers have when teaching computer science is the teaching of programming languages.  
I would be willing to provide Continued Professional Development and help teachers become more proficient and confident.

Some schools in Suffolk do specialise in ICT, I have the hop that the whole of Suffolk could specialise in computing and in the future, our towns like Bury St Edmunds and Ipswich could vival the likes of Cambridge.

#### Tackling mental heath issues.

I have had a very challenging upbringing and childhood, these challenges have given me unique experience and insights when it comes to tackling mental heath issues.

I would be very happy to work together with schools in Suffolk to tackle.  
Wether it be by visiting schools and talking to students directly or supplying resources that can be used in lessons.


#### Skills for an improved economy.

I will work together with the Suffolk Chamber of Commerce and other business leaders in Suffolk to [improve skills across business sectors.](https://www.suffolkchamber.co.uk/support/business-voice/people-skills/skills-training/local-skills-improvement-plan-updates/)  
My particular focus will be in digital skills, which is highlighted as a particular skill set that needs improvement, but my office will work towards improvements in all areas.


:::