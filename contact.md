---
title: Contact
description: Contact me and let me know your concerns.
form: https://fabform.io/f/CPMPT_P
---

# Contact me

Let me know of any concerns or improvements you would like to see here in Suffolk or any thoughts you may have about my [priorities for Suffolk](priorities).  
I will be standing in the **Abbygate & Minden** division of Bury St Edmunds which can be seen on the map below.

Please use the form below to contact me or email: [office@luketobrien.uk](office@luketobrien.uk)